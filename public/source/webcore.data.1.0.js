// data

(function () {

    'use strict';

    var store,
        outer = true,
        readySend = false;

    var prefix = __.prefix,
        suffix = 'data-';

    prefix += suffix;

    var state = __.attr.state;

    var attr = {
        data: prefix + 'item',
        select: 'data-webcore--select-caption',
    };

    var inputChange = ['text', 'datetime', 'email', 'tel', 'search', 'url'];

    var fn = {

        init: function () {
            if (__.storage.data === void 0) __.storage.data = Object.create(null), __.prototype.data = __.data = fn.started;
        },

        started: function () {

            var options = arguments[0];

            if (__.is.workObject(options)) {

                if (__.storage.data.pattern === void 0) {
                    __.storage.data = fn.action.get.options();
                    fn.action.set.listener();
                    fn.tooltip = __.tooltip(__.is.workObject(options.tooltip) ? options.tooltip : void 0);
                }

                __.extend(__.storage.data.submit, options.submit);

                if (options.pattern !== void 0) {

                    var key;

                    for (key in options.pattern) {
                        options.pattern[key] = __.extend(fn.action.get.pattern(), options.pattern[key]);
                        __.storage.data.pattern[key] = options.pattern[key];
                    }

                }

                store = __.storage.data;

            }

        },

        tooltip: void 0,

        event: {

            cut: function (event) {

                var target = event.target,
                    name = target.name,
                    current = store.pattern[name];

                if (current && !current.cut) {
                    fn.action.set.tooltip(target, 'cut');
                    event.preventDefault();
                }

            },

            copy: function (event) {

                var target = event.target,
                    name = target.name,
                    current = store.pattern[name];

                if (current && !current.copy) {
                    fn.action.set.tooltip(target, 'copy');
                    event.preventDefault();
                }

            },

            blur: function (event) {
                var target = event.target;
                fn.tooltip.hide();
                if (target.hasAttribute(attr.data) && target.getAttribute(attr.data) === state[1]) target.setAttribute(attr.data, state[0]);
            },

            paste: function (event) {

                var target = event.target,
                    name = target.name,
                    current = store.pattern[name];

                if (current && !current.paste) {
                    fn.action.set.tooltip(target, 'paste');
                    event.preventDefault();
                }

            },

            focus: function () {

                var target = event.target,
                    pattern = store.pattern,
                    name = target.getAttribute('name');

                if (name && name.trim() !== '') {
                    if (pattern[name] !== void 0) {
                        target.setAttribute('autocomplete', state[0]);
                    }
                }

            },

            click: function (event) {

                var i = 0,
                    input = document.querySelectorAll('[' + attr.data + ']');

                if (input.length !== 0) {
                    for (; i < input.length; i++) {
                        input[i].setAttribute(attr.data, state[0]);
                    }
                }

            },

            submit: function (event) {

                var target = event.target,
                    data = Object.create(null),
                    formData = new FormData();

                var proto = fn.event.submit.prototype = {

                    init: function () {

                        proto.data();
                        event.preventDefault();

                        if (readySend) {
                            if (store.submit.url !== void 0) {
                                data = __.toJSON(data);
                                formData.append('data', data);
                                __.ajax({
                                    data: formData,
                                    url: store.submit.url,
                                    error: store.submit.error,
                                    success: store.submit.success,
                                });
                                readySend = false;
                                return;
                            }

                            if (__.is.function(store.submit.callback)) {
                                store.submit.callback(data, target);
                                return;
                            }
                        }

                    },

                    data: function () {

                        outer = true;

                        var input = target.getElementsByTagName('input'),
                            select = target.getElementsByTagName('select'),
                            textarea = target.getElementsByTagName('textarea'),
                            customizeSelect = target.querySelectorAll('[' + attr.select + ']');

                        proto.each(input);
                        proto.each(select);
                        proto.each(textarea);
                        proto.each(customizeSelect);

                    },

                    each: function (collection) {

                        if (outer) {

                            var i = 0,
                                nodesLength = collection.length - 1;

                            for (; i < collection.length; i++) {
                                if (outer) {
                                    readySend = false;
                                    fn.action.get.test(collection[i], data);
                                    if (i === nodesLength) readySend = true;
                                } else {
                                    break;
                                }
                            }

                            return;

                        }

                        readySend = false;

                    },

                };

                proto.init();

            },

            input: function (event) {

                var i = 0,
                    target = event.target,
                    name = target.name,
                    type = target.type.toLowerCase(),
                    current = store.pattern[name];

                if (current !== void 0) {
                    for (; i < inputChange.length; i++) {
                        if (type === inputChange[i]) {
                            var maxDigit = fn.validation.length.int(event, current);
                            if (maxDigit) fn.tooltip.hide(), target.setAttribute(attr.data, state[0]);
                        }
                    }
                }

            },

            keypress: function (event) {

                var target = event.target,
                    name = target.name,
                    current = store.pattern[name];

                if (current !== void 0) {

                    var change = fn.validation.change[current.change](event.charCode);

                    if (change) {
                        fn.action.set.tooltip(target, current.change);
                        event.preventDefault();
                        return;
                    }

                    var maxLength = fn.validation.length.max(event, current);

                    if (maxLength) fn.tooltip.hide(), target.setAttribute(attr.data, state[0]);

                }

            },

        },

        action: {

            get: {

                test: function (target, data) {

                    if (target.getAttribute(attr.select) !== null) {

                        var current = Object.create(null);

                        if (data.customizeSelect === void 0) data.customizeSelect = [];

                        current.text = 'Селект';
                        current.value = target.textContent.trim();
                        data.customizeSelect.push(current);

                        return;

                    }

                    var name = target.name.toLowerCase();
                    
                    if (store.pattern[name] === void 0) {
                        fn.action.get.data(target, data);
                        return;
                    }

                    if (store.pattern[name].required) {
                        fn.validation.required(target, name);
                        if (!outer) return;
                    }

                    if (store.pattern[name].length.min !== void 0) {
                        fn.validation.length.min(target, store.pattern[name]);
                        if (!outer) return;
                    }

                    if (store.pattern[name].regexp !== void 0) {
                        var result = fn.validation.regexp[store.pattern[name].regexp](target.value);
                        if (!result) {
                            outer = false;
                            fn.action.set.tooltip(target, 'regexp');
                        }
                        if (!outer) return;
                    }

                    fn.action.get.data(target, data);

                    return;

                },

                data: function (target, data) {

                    var name = target.name.toLowerCase(),
                        type = target.type.toLowerCase(),
                        value = target.value;

                    if (type.indexOf('-') !== -1) type = fn.action.get.type(type);

                    var current = Object.create(null);

                    var proto = this.data.prototype = {

                        init: function () {
                            if (store.pattern[name] === void 0) {
                                if (proto.value()) {
                                    current.text = type;
                                    if (data[type] === void 0) data[type] = [];
                                    data[type].push(current);
                                }
                                return;
                            }
                            if (proto.value()) {
                                current.text = store.pattern[name].text === void 0 ? type : store.pattern[name].text;
                                if (data[name] === void 0) data[name] = [];
                                data[name].push(current);
                            }
                        },

                        value: function () {
                            if (type === 'checkbox' || type === 'radio') {
                                if (!target.checked) return false;
                                current.value = target.getAttribute('value');
                                return true;
                            }
                            current.value = value;
                            return true;
                        },

                    };

                    if (value.trim().length !== 0) proto.init();

                },

                type: function (name) {
                    var i = 0,
                        arr = name.split('-');
                    for (; i < arr.length; i++) {
                        if (i !== 0) {
                            arr[i] = arr[i].charAt(0).toUpperCase() + arr[i].slice(1);
                        }
                    }
                    return arr.join('');
                },

                style: function (target, prop) {
                    var value = getComputedStyle(target)[prop];
                    return value.indexOf('px') !== -1 ? parseInt(value) : value;
                },

                coord: function (target) {
                    var pos = target.getBoundingClientRect(),
                        offsetTop = __.storage.pageY !== void 0 ? __.storage.pageY : pageYOffset;
                    return {
                        top: pos.top + offsetTop,
                        left: pos.left + pageXOffset,
                    }
                },

                options: function () {
                    return {
                        tooltip: Object.create(null),
                        pattern: Object.create(null),
                        submit: {
                            url: void 0,
                            error: void 0,
                            success: void 0,
                            callback: void 0,
                        },
                    }
                },

                pattern: function () {
                    return {
                        text: void 0,
                        change: 'all',
                        length: {
                            int: void 0,
                            min: void 0,
                            max: void 0,
                        },
                        cut: true,
                        copy: true,
                        paste: true,
                        regexp: void 0,
                        required: false,
                        __proto__: null,
                    }
                },

            },

            set: {

                listener: function () {
                    for (var key in fn.event) document.addEventListener(key, fn.event[key], true);
                },

                tooltip: function (target, text) {
                    /*fn.tooltip.show({
                        offset: {
                            top: fn.action.get.coord(target).top + fn.action.get.style(target, 'height'),
                            left: fn.action.get.coord(target).left,
                        },
                        text: fn.tooltip.text[text],
                    });
                    target.setAttribute(attr.data, state[1]);*/
                },

            },

        },

        validation: {

            change: {

                ru: function (code) {
                    return code >= 33 && code <= 125 || code === 8470 ? true : false;
                },

                en: function (code) {
                    return code >= 33 && code <= 64 || code >= 91 && code <= 96 || code >= 123 && code <= 126 || code >= 1040 && code <= 1103 ? true : false;
                },

                re: function (code) {
                    return code >= 34 && code <= 43 || code >= 47 && code <= 64 || code >= 91 && code <= 96 || code >= 123 && code <= 126 ? true : false;
                },

                all: function (code) {
                    return false;
                },

                int: function (code) {
                    return code >= 32 && code <= 47 || code >= 58 && code <= 125 || code >= 1040 && code <= 1103 || code === 8470 ? true : false;
                },

                phone: function (code) {
                    return code >= 33 && code <= 47 || code >= 58 && code <= 125 || code >= 1040 && code <= 1103 || code === 8470 ? true : false;
                },

            },

            regexp: {

                ru: function (value) {
                    return value.match(/[а-яё ]+/i);
                },

                en: function (value) {
                    return value.match(/[a-z ]+/i);
                },

                re: function (value) {
                    return value.match(/[а-яёa-z ]+/i);
                },

                int: function (value) {
                    return value.match(/[0-9 ]+/);
                },

                email: function (value) {
                    return value.match(/[а-яёa-z0-9_]+@[a-z_]+?\.[a-zа-яё]{2,6}/i);
                },

                phone: function (value) {
                    return value.match(/[0-9 \+\-\(\)]+/);
                },

            },

            length: {

                int: function (event, current) {
                    var target = event.target;
                    if (current.length.int !== void 0) {
                        var value = target.value * 1;
                        if (value >= current.length.int) {
                            target.value = current.length.int;
                            fn.action.set.tooltip(target, 'maxDigit');
                            event.preventDefault();
                            return false;
                        }
                    }
                    return true;
                },

                min: function (target, current) {

                    var value = target.value;

                    if (current.length.int) {
                        value = value * 1;
                        if (value < current.length.min) {
                            fn.action.set.tooltip(target, 'minDigit');
                            outer = false;
                        }
                        return;
                    }

                    if (value.length < current.length.min) {
                        fn.action.set.tooltip(target, 'minLength');
                        outer = false;
                        return;
                    }

                },

                max: function (event, current) {
                    var target = event.target;
                    if (current.length.max !== void 0) {
                        if (target.value.length >= current.length.max) {
                            fn.action.set.tooltip(target, 'maxLength');
                            event.preventDefault();
                            return false;
                        }
                    }
                    return true;
                },

            },

            required: function (target, name) {

                var i = 0,
                    value = target.value,
                    type = target.type.toLowerCase();

                if (type === 'radio' || type === 'checkbox') {
                    var input = target.closest('form').querySelectorAll('[name="' + name + '"]');
                    for (; i < input.length; i++) {
                        if (input[i].checked) {
                            return;
                        }
                    }
                    outer = false;
                    fn.action.set.tooltip(target, 'required');
                    return;
                }

                if (value.trim().length === 0) {
                    outer = false;
                    fn.action.set.tooltip(target, 'required');
                }

            },

        },

    };

    if (window.__ !== void 0) fn.init();

})(window, document);