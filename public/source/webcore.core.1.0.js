// core

(function () {

    'use strict';

    var style = '[data-webcore--reset-style],[data-webcore--reset-style] *{margin:0;padding:0;outline:none;box-sizing:border-box}[data-webcore--tooltip-cover]{top:20px;left:20px;width:200px;z-index:100;cursor:pointer;background:#fff;position:absolute;border-radius:2px;box-shadow:0 3px 10px 0 rgba(0,0,0,.3)}[data-webcore--tooltip-box]{padding:15px;position:relative}[data-webcore--tooltip-box]::before{content:"";position:absolute;top:-7px;left:10px;width:20px;height:20px;background:#fff;border-radius:2px;transform:rotate(45deg)}[data-webcore--tooltip-text]{z-index:2;color:#333;margin:0;padding:0;font-size:14px;font-weight:500;font-style:initial;text-align:left;line-height:1.3em;position:relative;font-family:inherit}[data-webcore--animate]{transition-duration:.3s;transition-property:opacity,transform,width,height;transition-timing-function:ease-out}[data-webcore--animate="fadeOut"]{opacity:0;pointer-events:none}[data-webcore--animate="fadeIn"]{opacity:1;pointer-events:all}[data-webcore--animate="zoomOut"]{opacity:0;transform:scale(.7);pointer-events:none}[data-webcore--animate="zoomIn"]{opacity:1;transform:scale(1);pointer-events:all}[data-webcore--animate="slideOutUp"]{opacity:0;pointer-events:none;transform:translate3d(0,-50%,0)}[data-webcore--animate="slideInUp"]{opacity:1;pointer-events:all;transform:translate3d(0,0,0)}[data-webcore--animate="slideOutLeft"]{opacity:0;pointer-events:none;transform:translate3d(-50%,0,0)}[data-webcore--animate="slideInLeft"]{opacity:1;pointer-events:all;transform:translate3d(0,0,0)}[data-webcore--animate="slideOutRight"]{opacity:0;pointer-events:none;transform:translate3d(50%,0,0)}[data-webcore--animate="slideInRight"]{opacity:1;pointer-events:all;transform:translate3d(0,0,0)}[data-webcore--animate="slideOutDown"]{opacity:0;pointer-events:none;transform:translate3d(0,50px,0)}[data-webcore--animate="slideInDown"]{opacity:1;pointer-events:all;transform:translate3d(0,0,0)}[data-webcore--animate="rotateOutUp"]{opacity:0;transform:rotateX(10deg);transform-origin:bottom}[data-webcore--animate="rotateInUp"]{opacity:1;transform:rotateX(0);transform-origin:bottom}[data-webcore--animate="rotateOutLeft"]{opacity:0;transform:rotateY(-10deg);transform-origin:80%}[data-webcore--animate="rotateInLeft"]{opacity:1;transform:rotateY(0);transform-origin:80%}[data-webcore--animate="rotateOutRight"]{opacity:0;transform:rotateY(10deg);transform-origin:20%}[data-webcore--animate="rotateInRight"]{opacity:1;transform:rotateY(0);transform-origin:20%}[data-webcore--animate="rotateOutDown"]{opacity:0;transform:rotateX(-10deg);transform-origin:top}[data-webcore--animate="rotateInDown"]{opacity:1;transform:rotateX(0);transform-origin:top}';

    var prefix = 'data-webcore--';

    var suffix = {
        __proto__: null,
        animate: 'animate',
        reset: 'reset-style',
        scroll: 'body-scroll',
        tooltip: 'tooltip',
    };

    var fn = {

        __proto__: null,

        prefix: prefix,

        suffix: suffix,

        storage: Object.create(null),

        create: {

            __proto__: null,

            css: function () {
                var fragment = document.createElement('style'),
                    text = fn.is.string(arguments[0]) ? arguments[0] : '';
                if (arguments[1]) fragment.classList.add(arguments[1]);
                fragment.innerHTML = text;
                return document.head.appendChild(fragment);
            },

        },

        timing: {

            __proto__: null,

            linear: function () {
                return arguments[0];
            },

            bounce: function () {
                for (var a = 0, b = 1, result; 1; a += b, b /= 2) {
                    if (arguments[0] >= (7 - 4 * a) / 11) {
                        return -Math.pow((11 - 6 * a - 11 * arguments[0]) / 4, 2) + Math.pow(b, 2);
                    }
                }
            },

            easeInQuad: function () {
                var t = arguments[0];
                return t * t;
            },

            easeOutQuad: function () {
                var t = arguments[0];
                return t * (2 - t);
            },

            easeInOutQuad: function () {
                var t = arguments[0];
                return t < .5 ? 2 * t * t : -1 + (4 - 2 * t) * t;
            },

            easeInCubic: function () {
                var t = arguments[0];
                return t * t * t;
            },

            easeOutCubic: function () {
                var t = arguments[0];
                return (--t) * t * t + 1;
            },

            easeInOutCubic: function () {
                var t = arguments[0];
                return t < .5 ? 4 * t * t * t : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1;
            },

            easeInQuart: function () {
                var t = arguments[0];
                return t * t * t * t;
            },

            easeOutQuart: function () {
                var t = arguments[0];
                return 1 - (--t) * t * t * t;
            },

            easeInOutQuart: function () {
                var t = arguments[0];
                return t < .5 ? 8 * t * t * t * t : 1 - 8 * (--t) * t * t * t;
            },

            easeInQuint: function () {
                var t = arguments[0];
                return t * t * t * t * t;
            },

            easeOutQuint: function () {
                var t = arguments[0];
                return 1 + (--t) * t * t * t * t;
            },

            easeInOutQuint: function () {
                var t = arguments[0];
                return t < .5 ? 16 * t * t * t * t * t : 1 + 16 * (--t) * t * t * t * t;
            },

        },

        attr: {
            __proto__: null,
            body: {
                __proto__: null,
                scroll: prefix + suffix.scroll,
            },
            animate: prefix + suffix.animate,
            reset: prefix + suffix.reset,
            state: ['off', 'on'],
        },

        support: {

            __proto__: null,

            init: function () {
                var key;
                for (key in fn.support)
                    if (key !== 'init') fn.support[key]();
            },

            matches: function () {
                if (!Element.prototype.matches) {
                    Element.prototype.matches = Element.prototype.matchesSelector || Element.prototype.webkitMatchesSelector || Element.prototype.mozMatchesSelector || Element.prototype.msMatchesSelector;
                }
            },

            closest: function () {
                if (!Element.prototype.closest) {
                    Element.prototype.closest = function (selector) {
                        if (!this) return null;
                        if (this.matches(selector)) return this;
                        return !this.parentElement ? null : this.parentElement.closest(selector);
                    }
                }
            },

            includes: function () {
                if (!Array.prototype.includes) {
                    Object.defineProperty(Array.prototype, 'includes', {
                        value: function (searchElement, fromIndex) {
                            if (this == null) {
                                throw new TypeError('"this" is null or not defined');
                            }
                            var o = Object(this);
                            var len = o.length >>> 0;
                            if (len === 0) {
                                return false;
                            }
                            var n = fromIndex | 0;
                            var k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);

                            function sameValueZero(x, y) {
                                return x === y || (typeof x === 'number' && typeof y === 'number' && isNaN(x) && isNaN(y));
                            }
                            while (k < len) {
                                if (sameValueZero(o[k], searchElement)) {
                                    return true;
                                }
                                k++;
                            }
                            return false;
                        }
                    });
                }
            },

        },

        is: {

            __proto__: null,

            typeObject: function () {
                return {}.toString.call(arguments[0]).slice(8, -1).toLowerCase();
            },

            string: function () {
                return typeof arguments[0] === 'string' ? true : false;
            },

            number: function () {
                return typeof arguments[0] === 'number' ? true : false;
            },

            boolean: function () {
                return typeof arguments[0] === 'boolean' ? true : false;
            },

            function: function () {
                return fn.is.typeObject(arguments[0]) === 'function' ? true : false;
            },

            object: function () {
                return fn.is.typeObject(arguments[0]) === 'object' ? true : false;
            },

            date: function () {
                return fn.is.typeObject(arguments[0]) === 'date' ? true : false;
            },

            array: function () {
                return fn.is.typeObject(arguments[0]) === 'array' ? true : false;
            },

            nodelist: function () {
                return fn.is.typeObject(arguments[0]) === 'nodelist' ? true : false;
            },

            htmlcollection: function () {
                return fn.is.typeObject(arguments[0]) === 'htmlcollection' ? true : false;
            },

            element: function () {
                return fn.is.typeObject(arguments[0]).indexOf('element') !== -1 ? true : false;
            },

            exist: function () {
                return arguments[0] !== void 0 && arguments[0] !== null ? true : false;
            },

            main: function () {
                return arguments[0] === document || arguments[0] === window ? true : false;
            },

            emptyObject: function () {
                var key;
                for (key in arguments[0]) return false;
                return true;
            },

            emptyList: function () {
                return fn.is.list(arguments[0]) && arguments[0].length === 0 ? true : false;
            },

            list: function () {
                return fn.is.array(arguments[0]) || fn.is.nodelist(arguments[0]) || fn.is.htmlcollection(arguments[0]) ? true : false;
            },

            html: function () {

                switch (true) {
                    case fn.is.array(arguments[0]) && !fn.is.emptyList(arguments[0]):
                        var i = arguments[0].length - 1
                        for (; i >= 0; i--)
                            if (!fn.is.element(arguments[0][i])) return false;
                        return true;
                        break;
                    case fn.is.nodelist(arguments[0]) || fn.is.htmlcollection(arguments[0]):
                        return !fn.is.emptyList(arguments[0]) ? true : false;
                        break;
                    case fn.is.element(arguments[0]):
                        return true;
                        break;
                    default:
                        return false;
                }

            },

            text: function () {
                return fn.is.typeObject(arguments[0]) === 'text' ? true : false;
            },

            workObject: function () {
                return fn.is.object(arguments[0]) && !fn.is.emptyObject(arguments[0]) ? true : false;
            },

            touch: function () {
                return 'ontouchstart' in document;
            },

            portrait: function () {
                return fn.viewport().width < fn.viewport().height ? true : false;
            },

        },

        effect: {
            __proto__: null,
            fade: ['fadeOut', 'fadeIn'],
            zoom: ['zoomOut', 'zoomIn'],
            rotateUp: ['rotateOutUp', 'rotateInUp'],
            rotateLeft: ['rotateOutLeft', 'rotateInLeft'],
            rotateRight: ['rotateOutRight', 'rotateInRight'],
            rotateDown: ['rotateOutDown', 'rotateInDown'],
            slideUp: ['slideOutUp', 'slideInUp'],
            slideLeft: ['slideOutLeft', 'slideInLeft'],
            slideRight: ['slideOutRight', 'slideInRight'],
            slideDown: ['slideOutDown', 'slideInDown'],
        },

        init: function () {
            if (fn.is.exist(arguments[0])) {
                switch (true) {
                    case fn.is.function(arguments[0]):
                        fn.ready(void 0, arguments[0]);
                        break;
                    case fn.is.workObject(arguments[0]):
                        for (var key in arguments[0]) fn.ready(key, arguments[0][key]);
                        break;
                }
            }
        },

        ready: function () {

            var state = arguments[0],
                callback = arguments[1];

            var proto = this.ready.prototype = {

                __proto__: null,

                state: state === 'interactive' || state === 'complete' ? arguments[0] : 'interactive',

                init: function () {
                    if (fn.is.function(callback)) document.addEventListener('readystatechange', proto.event.readystatechange);
                },

                event: {
                    readystatechange: function () {
                        if (document.readyState === proto.state) return callback();
                    },
                },

            };

            proto.init();

        },

        export: function () {
            if (fn.is.exist(arguments[0]) && fn.is.workObject(arguments[1]))
                for (var key in arguments[1]) arguments[0][key] = arguments[1][key];
            return arguments[0];
        },

        screen: function () {
            return {
                __proto__: null,
                width: window.screen.width,
                height: window.screen.height,
                availWidth: window.screen.availWidth,
                availHeight: window.screen.availHeight,
                pixelDepth: window.screen.pixelDepth,
                colorDepth: window.screen.colorDepth,
            }
        },

        viewport: function () {
            return {
                __proto__: null,
                width: document.documentElement.clientWidth,
                height: document.documentElement.clientHeight,
            }
        },

        timeout: function () {
            switch (true) {
                case fn.is.number(arguments[0]) && fn.is.function(arguments[1]):
                    return setTimeout(arguments[1], arguments[0]);
                    break;
                case fn.is.function(arguments[0]) && !fn.is.exist(arguments[1]):
                    return setTimeout(arguments[0], 10);
                    break;
            }
        },

        interval: function () {
            if (fn.is.number(arguments[0]) && fn.is.function(arguments[1])) return setInterval(arguments[1], arguments[0]);
        },

        toJSON: function () {

            var object = arguments[0],
                indent = arguments[1];

            var proto = this.toJSON.prototype = {

                __proto__: null,

                init: function () {
                    if (fn.is.workObject(object)) return proto.transform(proto.each(object, proto.result));
                },

                each: function () {

                    var data = arguments[0],
                        expandable = arguments[1];

                    if (fn.is.list(data) && !fn.is.emptyList(data)) {
                        var i;
                        for (i = 0; i < data.length; i++) {
                            var current = proto.test(data[i], expandable);
                            if (fn.is.exist(current)) expandable.push(current);
                        }
                    } else {
                        var key;
                        for (key in data) {
                            var current = proto.test(data[key], expandable);
                            if (fn.is.exist(current)) expandable[key] = current;
                        }
                    }

                    return expandable;

                },

                test: function () {

                    var item = arguments[0],
                        expandable = arguments[1];

                    if (!fn.is.function(item) && !fn.is.main(item)) {
                        switch (true) {
                            case fn.is.list(item) && !fn.is.emptyList(item):
                                return proto.each(item, []);
                                break;
                            case fn.is.workObject(item):
                                return proto.each(item, Object.create(null));
                                break;
                            default:
                                return item;
                        }
                    }

                },

                transform: function () {
                    return fn.is.number(indent) ? JSON.stringify(arguments[0], '', indent) : JSON.stringify(arguments[0]);
                },

                result: Object.create(null),

            };

            return proto.init();

        },

        fromJSON: function () {
            if (fn.is.string(arguments[0])) return JSON.parse(arguments[0], fn.is.function(arguments[1]) ? arguments[1] : void 0);
        },

        extend: function () {

            var proto = this.extend.prototype = {

                init: function () {

                    var options = fn.is.workObject(arguments[0]) ? arguments[0] : Object.create(null),
                        defaults = fn.is.workObject(arguments[1]) ? arguments[1] : Object.create(null);

                    return proto.each(options, defaults);

                },

                each: function (options, defaults) {

                    if (fn.is.list(defaults) && !fn.is.emptyList(defaults)) {
                        var i;
                        for (i = 0; i < defaults.length; i++) options.push(defaults[i]);
                    } else {
                        var key;
                        for (key in defaults) {
                            if (options === void 0) {
                                options = defaults;
                                continue;
                            }
                            options[key] = proto.test(options[key], defaults[key]);
                        }
                    }

                    return options;

                },

                test: function (options, defaults) {
                    return fn.is.workObject(defaults) ? proto.each(options, defaults) : defaults;
                },

            };

            return proto.init(arguments[0], arguments[1]);

        },

        browser: function () {

            var proto = this.browser.prototype = {

                __proto__: null,

                ua: window.navigator.userAgent.toLowerCase(),

                core: function () {

                    switch (true) {
                        case proto.ua.indexOf('trident') !== -1:
                            return 'trident';
                            break;
                        case proto.ua.indexOf('firefox') !== -1:
                            return 'gecko';
                            break;
                        case proto.ua.indexOf('edge') !== -1:
                            return 'edgehtml';
                            break;
                        case proto.ua.indexOf('chrome') === -1 && proto.ua.indexOf('safari') !== -1:
                            return 'safari';
                            break;
                        case proto.ua.indexOf('webkit') !== -1:
                            return 'webkit';
                            break;
                    }

                },

            };

            return {
                __proto__: null,
                data: proto.ua,
                core: proto.core(),
                cookie: window.navigator.cookieEnabled,
            }

        },

        cookie: function () {

            var proto = this.cookie.prototype = {

                __proto__: null,

                init: function () {

                    switch (true) {
                        case fn.is.exist(arguments[0]):
                            switch (true) {
                                case fn.is.workObject(arguments[0]):
                                    var key;
                                    proto.options = fn.extend(proto.options, fn.is.workObject(arguments[1]) ? arguments[1] : {});
                                    proto.options.expires = proto.expires(proto.options.expires);
                                    for (key in arguments[0]) fn.is.exist(arguments[0][key]) ? proto.action.set(key, arguments[0][key]) : proto.action.remove(key);
                                    return document.cookie;
                                    break;
                                case fn.is.string(arguments[0]):
                                    return proto.action.get(arguments[0]);
                                    break;
                            }
                            break;
                        case arguments[0] === null:
                            proto.action.clear();
                            break;
                        default:
                            return document.cookie;
                    }

                },

                options: {
                    path: '/',
                    domain: '',
                    expires: '',
                    secure: true,
                },

                expires: function () {
                    arguments[0] = fn.is.exist(arguments[0]) && fn.is.number(arguments[0]) ? arguments[0] : 1;
                    var date = new Date;
                    date.setDate(date.getDate() + arguments[0]);
                    return date.toUTCString();
                },

                action: {

                    get: function () {
                        var matches = document.cookie.match(new RegExp('(?:^|; )' + arguments[0].replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + '=([^;]*)'));
                        return matches ? decodeURIComponent(matches[1]) : void 0;
                    },

                    set: function () {
                        var data = [arguments[0] + '=' + encodeURIComponent(arguments[1]), 'path=' + proto.options.path, 'domain=' + proto.options.domain, 'expires=' + proto.options.expires];
                        if (proto.options.secure) data.push('secure=' + proto.options.secure);
                        document.cookie = data.join('; ');
                    },

                    remove: function () {
                        document.cookie = arguments[0] + '=; path=/; expires=' + new Date(0).toUTCString();
                    },

                    clear: function () {
                        var i = 0,
                            cookie = document.cookie.split(';');
                        for (; i < cookie.length; i++) proto.action.remove(cookie[i].split('=')[0]);
                    },

                },

            };

            return proto.init(arguments[0], arguments[1]);

        },

        serialize: function () {
            if (fn.is.workObject(arguments[0])) {
                var key, data = [];
                for (key in arguments[0]) data.push(encodeURIComponent(key) + '=' + encodeURIComponent(arguments[0][key]));
                return data.join('&');
            }
        },

        ajax: function () {

            var proto = this.ajax.prototype = {

                __proto__: null,

                mime: {
                    script: 'text/javascript, application/javascript, application/x-javascript',
                    json: 'application/json',
                    xml: 'application/xml, text/xml',
                    html: 'text/html',
                    text: 'text/plain',
                    ajax: 'XMLHttpRequest',
                    formPOST: 'multipart/form-data',
                    formGET: 'application/x-www-form-urlencoded',
                },

                header: {
                    ajax: 'X-Requested-With',
                    content: 'Content-Type',
                },

                options: {
                    type: 'POST',
                    data: {},
                    header: 'ajax',
                    charset: 'utf-8',
                    username: '',
                    password: '',
                    url: '',
                    success: '',
                    error: '',
                },

                init: function () {

                    proto.options = fn.extend(proto.options, fn.is.workObject(arguments[0]) ? arguments[0] : {});

                    if (fn.is.string(proto.options.url) && proto.options.url !== '') {
                        var Request = proto.action.create.request();
                        proto.action.open(Request);
                        proto.action.create.header(Request, proto.options.header === 'ajax' ? proto.header.ajax : proto.header.content, proto.mime[proto.options.header]);
                        Request.addEventListener('readystatechange', function () {
                            proto.action.progress(Request);
                        });
                        proto.options.type === 'POST' ? Request.send(proto.options.data) : Request.send(null);
                        return Request;
                    }

                },

                action: {

                    create: {

                        request: function () {
                            return new XMLHttpRequest();
                        },

                        header: function () {
                            return arguments[0].setRequestHeader(arguments[1], arguments[2] + '; charset=' + proto.options.charset);
                        },

                    },

                    open: function () {
                        return arguments[0].open(proto.options.type, proto.options.url, true, proto.options.username, proto.options.password);
                    },

                    progress: function () {
                        if (arguments[0].readyState === 4) {
                            if (arguments[0].status >= 200 && arguments[0].status <= 203) {
                                if (fn.is.function(proto.options.success)) proto.options.success(arguments[0]);
                            } else {
                                if (fn.is.function(proto.options.error)) proto.options.error(arguments[0]);
                            }
                        }
                    },

                },

            };

            return proto.init(arguments[0]);

        },

        animate: function () {

            var fraction, progress, start = performance.now();

            this.animate.Instance = function () {

                var proto = this.__proto__ = {

                    options: {
                        queue: true,
                        scrolled: true,
                        duration: 2000,
                        timing: 'easeInOutQuad',
                        progress: void 0,
                        ending: void 0,
                        starting: void 0,
                    },

                    init: function () {
                        var options = fn.extend(proto.options, fn.is.workObject(arguments[0]) ? arguments[0] : {});
                        options.timing = fn.timing[options.timing] === void 0 ? fn.timing.easeInOutQuad : fn.timing[options.timing];
                        return options;
                    },

                };
                return proto.init(arguments[0]);

            };

            var proto = {

                __proto__: null,

                init: function () {
                    if (fn.queue === void 0) fn.queue = [];
                    fn.queue.length === 0 ? arguments[0].state = true : arguments[0].state = false;
                    if (arguments[0].queue || fn.queue.length === 0) fn.queue.push(arguments[0]);
                    proto.action.started(arguments[0]);
                },

                action: {

                    __proto__: null,

                    prevent: function (event) {
                        event.preventDefault();
                    },

                    passive: {
                        passive: false,
                    },

                    started: function () {
                        if (arguments[0].state) {
                            if (!arguments[0].scrolled) {
                                window.addEventListener('wheel', proto.action.prevent, proto.action.passive);
                            }
                            if (fn.is.function(arguments[0].starting)) arguments[0].starting();
                            requestAnimationFrame(proto.action.animate);
                        }
                    },

                    animate: function () {
                        fraction = fraction === 0 ? 1 : (arguments[0] - start) / fn.queue[0].duration;
                        progress = fn.queue[0].timing(fraction);
                        fn.queue[0].progress(progress);
                        if (fraction <= 1) {
                            requestAnimationFrame(proto.action.animate);
                        } else {
                            if (fraction !== 1) fn.queue[0].progress(fn.queue[0].timing(fraction = 1));
                            window.removeEventListener('wheel', proto.action.prevent, proto.action.passive);
                            if (fn.is.function(fn.queue[0].ending)) fn.queue[0].ending();
                            fn.queue.splice(0, 1);
                            if (fn.queue[0] !== void 0) {
                                fraction = progress = void 0;
                                start = performance.now();
                                fn.queue[0].state = true;
                                proto.action.started(fn.queue[0]);
                            }
                        }
                    },

                },

            };

            proto.init(new this.animate.Instance(arguments[0]));

        },

        scroll: function () {

            var props = ['top', 'left', 'right', 'bottom', 'position'];

            var distance = document.scrollingElement || document.documentElement;

            var proto = this.scroll.prototype = {

                __proto__: null,

                init: function () {
                    if (fn.is.exist(arguments[0])) proto.set(arguments[0], arguments[1]);
                },

                set: function () {
                    var x = fn.is.exist(arguments[1]) ? arguments[1] : 0,
                        y = arguments[0];
                    setTimeout(function () {
                        window.scrollTo(x, y);
                    }, 50);
                },

            };

            var options = {
                queue: false,
                scrolled: false,
                duration: 2000,
                timing: 'easeInOutQuad',
                ending: void 0,
                starting: void 0,
            };

            var methods = {

                __proto__: null,

                top: distance.scrollTop,

                left: distance.scrollLeft,

                on: function () {
                    var i = 0;
                    for (; i < props.length; i++) props[i] === 'top' || props[i] === 'position' ? document.body.style[props[i]] = '' : document.body.style[props[i]] = '0px';
                    window.scrollTo(0, fn.storage.scrolltop === void 0 ? 0 : parseInt(fn.storage.scrolltop.replace(/-/, '')));
                    document.body.setAttribute(fn.attr.body.scroll, fn.attr.state[1]);
                },

                off: function () {
                    if (fn.storage.scrolltop === void 0) fn.storage.scrolltop = 0;
                    fn.storage.scrolltop = -distance.scrollTop + 'px';
                    var i = 0;
                    for (; i < props.length; i++) {
                        switch (props[i]) {
                            case 'position':
                                document.body.style[props[i]] = 'fixed';
                                break;
                            case 'top':
                                document.body.style[props[i]] = fn.storage.scrolltop;
                                break;
                            default:
                                document.body.style[props[i]] = '0px';
                        }
                    }
                    document.body.setAttribute(fn.attr.body.scroll, fn.attr.state[0]);

                },

                to: function () {

                    var docOffset = distance.scrollTop;
                    options = fn.extend(options, fn.is.workObject(arguments[0]) ? arguments[0] : {});

                    if (fn.is.number(options.offset) && docOffset !== options.offset) {
                        options.progress = function (progress) {
                            fn.scroll((options.offset - docOffset) * progress + docOffset);
                        };
                        fn.animate(options);
                    }

                },

                up: function () {

                    var docOffset = distance.scrollTop;

                    if (docOffset !== 0) {
                        options = fn.extend(options, fn.is.workObject(arguments[0]) ? arguments[0] : {});
                        options.progress = function (progress) {
                            fn.scroll((0 - docOffset) * progress + docOffset);
                        };
                        fn.animate(options);
                    }

                },

                down: function () {

                    var docOffset = distance.scrollTop,
                        screenHeight = fn.viewport().height,
                        docHeight = parseInt(getComputedStyle(document.documentElement).height);

                    if (docOffset + screenHeight !== docHeight) {
                        options = fn.extend(options, fn.is.workObject(arguments[0]) ? arguments[0] : {});
                        options.progress = function (progress) {
                            fn.scroll((docHeight - (docOffset + screenHeight)) * progress + docOffset);
                        };
                        fn.animate(options);
                    }

                },

                key: function () {

                    var proto = this.key.prototype = {

                        __proto__: null,

                        init: function () {
                            if (fn.storage.shortcutKey === void 0) fn.storage.shortcutKey = 0;
                            if (fn.storage.shortcutKey === 0) {
                                var key;
                                fn.storage.shortcutKey++;
                                options = fn.extend(options, fn.is.workObject(arguments[0]) ? arguments[0] : {});
                                for (key in proto.event) document.addEventListener(key, proto.event[key]);
                            }
                        },

                        event: {

                            click: function (event) {
                                if (event.ctrlKey) {
                                    fn.scroll().up(options);
                                    event.preventDefault();
                                }
                            },

                            contextmenu: function (event) {
                                if (event.ctrlKey) {
                                    fn.scroll().down(options);
                                    event.preventDefault();
                                }
                            },

                            keyup: function (event) {
                                if (event.ctrlKey && event.keyCode === 38) fn.scroll().up(options);
                                if (event.ctrlKey && event.keyCode === 40) fn.scroll().down(options);
                            },

                        },

                    };

                    proto.init(arguments[0]);

                },

            };

            proto.init(arguments[0], arguments[1]);

            return methods;

        },

        tooltip: function () {

            var proto = this.tooltip.prototype = {

                __proto__: null,

                init: function () {
                    if (fn.storage.tooltip === void 0) fn.storage.tooltip = 0;
                    if (fn.storage.tooltip === 0) {
                        fn.storage.tooltip++;
                        fn.attr.tooltip = proto.attr;
                        options = fn.extend(options, fn.is.workObject(arguments[0]) ? arguments[0] : {});
                        options.effect = fn.effect[options.effect] === void 0 ? fn.effect.slideDown : fn.effect[options.effect];
                        proto.create();
                        proto.tooltip.cover.style.zIndex = options.zIndex;
                        proto.tooltip.cover.style.transitionDuration = options.duration + 'ms';
                        for (var key in proto.event) document.addEventListener(key, proto.event[key]);
                    }
                },

                attr: {
                    __proto__: null,
                    cover: fn.prefix + suffix.tooltip + '-cover',
                    box: fn.prefix + suffix.tooltip + '-box',
                    text: fn.prefix + suffix.tooltip + '-text',
                },

                tooltip: {
                    box: void 0,
                    text: void 0,
                    cover: void 0,
                },

                event: {

                    click: function (event) {
                        if (event.target.closest('[' + fn.attr.tooltip.cover + ']')) methods.hide();
                    },

                },

                create: function () {
                    var markup = '<div ' + fn.attr.reset + '="" ' + proto.attr.cover + '="" ' + fn.attr.animate + '="' + options.effect[0] + '"><div ' + proto.attr.box + '=""><p ' + proto.attr.text + '=""></p></div></div>'
                    document.body.insertAdjacentHTML('beforeEnd', markup);
                    proto.tooltip.cover = document.querySelector('[' + proto.attr.cover + ']');
                    proto.tooltip.box = document.querySelector('[' + proto.attr.box + ']');
                    proto.tooltip.text = document.querySelector('[' + proto.attr.text + ']');
                },

            };

            var options = {
                offset: 5,
                zIndex: 100,
                duration: 300,
                effect: 'slideDown',
            };

            var methods = {

                __proto__: null,

                add: function () {
                    if (fn.is.workObject(arguments[0])) fn.extend(methods.text, arguments[0]);
                },

                text: {
                    en: 'Допустима только латиница',
                    ru: 'Допустима только кириллица',
                    re: 'Допустима латиница и кириллица',
                    int: 'Допустимы только цифры',
                    phone: 'Допустимы только цифры и пробел',
                    maxDigit: 'Достигнуто максимальное число',
                    minDigit: 'Необходимо ввести большее число',
                    maxLength: 'Достигнуто максимальное количество вводимых символов',
                    minLength: 'Необходимо ввести больше символов',
                    required: 'Это поле обязательно для заполнения',
                    cut: 'Невозможно вырезать данные из поля ввода',
                    copy: 'Невозможно скопировать данные из поля ввода',
                    paste: 'Невозможно вставить данные в поле ввода',
                    regexp: 'Проверьте правильность заполнения поля',
                },

                show: function () {
                    if (fn.is.workObject(arguments[0])) {
                        if (fn.is.workObject(arguments[0].offset))
                            for (var key in arguments[0].offset) proto.tooltip.cover.style[key] = arguments[0].offset[key] + options.offset + 'px',
                                proto.tooltip.cover.setAttribute(fn.attr.animate, options.effect[1]);
                        proto.tooltip.text.innerHTML = fn.is.string(arguments[0].text) ? arguments[0].text : '';
                    }
                },

                hide: function () {
                    proto.tooltip.cover.setAttribute(fn.attr.animate, options.effect[0]);
                },

            };

            proto.init(arguments[0]);

            return methods;

        },

    };

    var fnExport = {

        core: function () {
            return fn.init(arguments[0]);
        },

        export: function () {
            var key;
            fn.support.init();
            fn.create.css(style, 'webcore-style--common');
            fnExport.core.prototype = fn;
            fn.export(fnExport.core, fn);
            window.__ = fnExport.core;
        },

    };

    fnExport.export();

})(window, document);