const ShowGaleryInfo = {
		"xl": {
			"start": 10,
			"row": 7
		},
		"md": {
			"start": 7,
			"row": 5
		},
		"sm": {
			"start": 3,
			"row": 4
		}
	},
	formData = {
		call: {
			title: "Введите ваши данные",
			subTitle: "И мы вам перезвоним",
			selectProduct: false,
			selectOrder: false
		},
		buy: {
			title: "Приобрести робота",
			subTitle: "Чтобы приобретсти или купить робота заполните форму",
			selectProduct: true,
			selectOrder: false
		},
		rent: {
			title: "Аренда робота",
			subTitle: "Чтобы взять в аренду робота заполните форму",
			selectProduct: true,
			selectOrder: true
		},
		default: {
			title: "аренда",
			subTitle: "Чтобы взять в аренду робота заполните форму",
			selectProduct: true,
			selectOrder: true
		},
		elements: {
			selectProduct: null,
			selectOrder: null
		}
	}

let countShowGalery;

function showMenu() {
	document.querySelector('.menu-list').classList.toggle('menu-list_hidden');
}

function showModal(modal) {
	modal.classList.remove('modal_hidden');
	if (modal.classList.contains('modal-thank')) {
		setTimeout(function () {
			modal.classList.add('modal_hidden');
		}, 3500);
	}
};

function showFormModal(data, modal) {

	let option = data.split(' ');

	let title = modal.querySelector('.modal__title'),
		subTitle = modal.querySelector('.modal__subtitle');

	title.textContent = formData[option[0]].title;
	subTitle.textContent = formData[option[0]].subTitle;

	if (formData[option[0]].selectProduct) {
		if (!modal.querySelector('.select-cover').querySelector('.selectProduct')) {
			modal.querySelector('.select-cover').appendChild(formData.elements.selectProduct)
		}
	} else {
		if (modal.querySelector('.select-cover').querySelector('.selectProduct')) {
			formData.elements.selectProduct = modal.querySelector('.select-cover').querySelector('.selectProduct');
			modal.querySelector('.select-cover').querySelector('.selectProduct').remove();
		}
	}

	if (formData[option[0]].selectOrder) {
		if (!modal.querySelector('.select-cover').querySelector('.selectOrder')) {
			modal.querySelector('.select-cover').appendChild(formData.elements.selectOrder)
		}
	} else {
		if (modal.querySelector('.select-cover').querySelector('.selectOrder')) {
			formData.elements.selectOrder = modal.querySelector('.select-cover').querySelector('.selectOrder');
			modal.querySelector('.select-cover').querySelector('.selectOrder').remove();
		}
	}

	if (option[1]) {
		modal.querySelector('[data-formdata-option="' + option[1] + '"]').selected = true;
	}

	if (option[2]) {
		modal.querySelector('[data-formdata-option="' + option[2] + '"]').selected = true;
	}

}

function closeModal(event) {
	let modal;

	if (!event.classList) {
		if (event.target.classList.contains('modal') ||
			event.target.classList.contains('close_modal')) {
			modal = event.target.closest('.modal');
		}
	} else {
		modal = event;
	}
	if (modal)
		modal.classList.add('modal_hidden');
};

function scrollTo(event) {
	if (this.getAttribute('href')[0] == '#') {

		event.preventDefault();

		$('body,html').animate({
			scrollTop: $($(this).attr('href')).offset().top - 100
		}, 600);
	}
};

function showHeader() {
	if (this.window.pageYOffset > 10) {
		this.document.querySelector('header').classList.add('bg_blue');
		return;
	}
	if (this.window.pageYOffset < 10) {
		this.document.querySelector('header').classList.remove('bg_blue');
	}
};

function showGalery(event) {
	let target = event.target.closest('.gallery__btn'),
		showCount = countShowGalery.row;


	if (target.parentElement.classList.contains('pg-gallery')) {
		showCount = 6
	}

	for (var i = 0; i < showCount; i++) {
		if (!!target.parentElement.querySelector('.gallery__item-hidden')) {
			let hidImg = target.parentElement.querySelector('.gallery__item-hidden');

			if (hidImg.querySelector('img').getAttribute('data-src'))
				hidImg.querySelector('img').setAttribute('src', hidImg.querySelector('img').getAttribute('data-src'));

			hidImg.classList.remove('gallery__item-hidden');
		} else {
			target.classList.add('btn_hidden');
		}

		if (!target.parentElement.querySelector('.gallery__item-hidden')) target.classList.add('btn_hidden');
	}
	$('body,html').animate({
		scrollTop: $(target).offset().top + -screen.height / 3
	}, 1100);
};

function hideGalery(count) {
	document.querySelectorAll('[data-gallery]').forEach(function (element) {
		element.querySelectorAll('.gallery__item').forEach(function (element, i) {


			if (i > count && !element.classList.contains('gallery__item-hidden')) {
				img = element.querySelector('img');
				img.setAttribute('data-src', img.getAttribute('src'));
				img.setAttribute('src', '');
				element.classList.add('gallery__item-hidden');
			}
		})
	})
};

function isVisible(target) {
	let targetPosition = {
			top: window.pageYOffset + target.getBoundingClientRect().top,
			bottom: window.pageYOffset + target.getBoundingClientRect().bottom
		},
		windowPosition = {
			top: window.pageYOffset,
			bottom: window.pageYOffset + document.documentElement.clientHeight
		};

	if (targetPosition.bottom > windowPosition.top && // Если позиция нижней части элемента больше позиции верхней чайти окна, то элемент виден сверху
		targetPosition.top < windowPosition.bottom) { // Если позиция верхней части элемента меньше позиции нижней чайти окна, то элемент виден снизу

		// Если элемент полностью видно, то запускаем следующий код
		return true;
	} else {
		// Если элемент не видно, то запускаем этот код
		return false;
	};
};

function animAdd(event) {
	document.querySelectorAll('[data-anim]').forEach(function (element) {
		if (isVisible(element)) {
			element.classList.add(element.getAttribute('data-anim'));
		}
	})
}

function accord() {
	document.querySelectorAll('[data-accord-title]').forEach(function (elem) {

		elem.nextElementSibling.style.maxHeight = (+elem.nextElementSibling.scrollHeight + 50) + 'px';


		elem.addEventListener('click', function (event) {
			this.classList.toggle('hidden');
		})
	});
}

document.addEventListener('DOMContentLoaded', function () {

	if (!document.querySelector('.started')) {

		document.querySelector('header').classList.add('bg_form');

		const startedMargin = setInterval(() => {
			document.querySelector('header').nextElementSibling.style.marginTop = getComputedStyle(document.querySelector('header')).height;
		}, 1);

		setTimeout(function () {
			clearInterval(startedMargin);
		}, 500);
	}

	window.addEventListener('scroll', showHeader);
	window.addEventListener('scroll', animAdd);

	document.querySelectorAll('.gallery__btn').forEach(element => element.addEventListener('click', showGalery));
	document.querySelectorAll('a').forEach((element) => element.addEventListener('click', scrollTo));
	document.querySelectorAll('[data-modal-link]').forEach(function (element) {
		element.addEventListener('click', function () {
			showModal(document.querySelector(element.getAttribute('data-modal-link')))
		});
	});

	document.querySelectorAll('[data-modal-formdata]').forEach(function (element) {
		element.addEventListener('click', function () {
			showFormModal(element.getAttribute('data-modal-formdata'), document.querySelector(element.getAttribute('data-modal-link')))
		});
	});

	document.querySelectorAll('.modal').forEach(element => element.addEventListener('click', closeModal));
	document.querySelector('.menu-btn').addEventListener('click', showMenu);

	document.querySelector('.input_mail').addEventListener('focus', function (event) {
		if (event.target.classList.contains('input_error')) {
			event.target.classList.remove('input_error');
		}
	});

	if (screen.width < 2560) {
		countShowGalery = ShowGaleryInfo["xl"];
	}
	if (screen.width < 992) {
		countShowGalery = ShowGaleryInfo["md"];
	}
	if (screen.width < 769) {
		countShowGalery = ShowGaleryInfo["sm"];
	}

	$('.news__slider').slick({
		infinite: false,
		dots: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		responsive: [{
				breakpoint: 769,
				settings: {
					slidesToShow: 1,
				}
			},
			{
				breakpoint: 520,
				settings: {
					slidesToShow: 1,
				}
			},
			{
				breakpoint: 321,
				settings: {
					slidesToShow: 1,
				}
			}
		]
	});

	document.querySelectorAll('.pg-about-reviews__text').forEach(function (elem) {
		elem.textContent = elem.textContent.substring(0, 140);
		elem.textContent = elem.textContent.substring(0, elem.textContent.lastIndexOf(' ')) + '...';
	})

	document.querySelectorAll('.pg-news .news__text').forEach(function (elem) {
		elem.textContent = elem.textContent.substring(0, 210);
		elem.textContent = elem.textContent.substring(0, elem.textContent.lastIndexOf(' ')) + '...';
	})

	document.querySelectorAll('.slick-slide').forEach(function (element, i) {
		element.addEventListener('click', function (event) {
			let slide = event.target.closest('.slick-slide');
			$('.news__slider').slick('slickGoTo', slide.getAttribute('data-slick-index'));
		})
	});

	hideGalery(countShowGalery.start);
	showHeader();
	animAdd();
	accord();
	const mobileMenu = document.getElementById('menunav');
	mobileMenu.addEventListener('toggleopen', () => document.body.style.overflow = 'hidden');
	mobileMenu.addEventListener('toggleclose', () => document.body.style.overflow = '');
});